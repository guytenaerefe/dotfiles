# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


if [ -f ~/.bash_passwords ]; then
    . ~/.bash_passwords
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
#source ~/.git-prompt.sh 
# Old prompt:
#PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '
# New prompt: (I like purple, man)
PS1='\[$(tput bold)\]\[$(tput setaf 4)\][\[$(tput setaf 5)\]\u\[$(tput setaf 4)\]@\[$(tput setaf 5)\]\h \[$(tput setaf 2)\]\W$(__git_ps1 " (%s)")\[$(tput setaf 4)\]]\\$ \[$(tput sgr0)\]'

export PATH="$PATH:$HOME/bin"
# NVM > NPM
export PATH="$PATH:$HOME/npm/bin"
export NODE_PATH="$NODE_PATH:$HOME/npm/lib/node_modules"

export NVM_DIR="/usr/local/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# SSH-AGENT
SSH_ENV=$HOME/.ssh/environment
   
# start the ssh-agent
function start_agent {
    echo "Initializing new SSH agent..."
    # spawn ssh-agent
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}
   
if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

# Git
alias gitc='ssh-add ~/.ssh/id_bitbucket'
alias nw='git diff -w --no-color | git apply --cached --ignore-whitespace'
alias gpush='git push origin master'
alias gpull='git pull origin master'
alias ups='git pull upstream master'
alias gp='git pull'
alias gl='git log --pretty=oneline --pretty="%h %cn %cr ---- %s"'
alias gl5='git log --pretty=oneline --pretty="%h %cn %cr ---- %s" | head -5'
alias gld='git log --pretty=oneline --pretty="%h %cn %cr ---- %s" --after="1 day ago"'
alias glh='git log --pretty=oneline --pretty="%h %cn %cr ---- %s" --after="1 hour ago"'
alias gls='git log --pretty=oneline --pretty="%h %cn %cr ---- %s" --stat'
alias glsr='git log --pretty=oneline --pretty="%h %cn %cr ---- %s" --stat --commiter="Francesco de Guytenaere"'
alias gr='git reflog --date=relative'
alias gbd='git branch -D'
alias gs='git status'
alias gd='git diff'
alias gdc='git diff --cached'
alias grc='git add -A && git rebase --continue'
alias gra='git rebase --abort'
alias gch='git checkout'
alias gcm='git commit -m'
alias gca='git commit -am'
alias gcaa='git commit -a --amend'
alias undo_stash_apply='git stash show -p | git apply --reverse'
alias gsave='git stash save'
alias ga='git add'
alias glg='gl --graph'
alias gpom='git push origin master'
alias gw='git whatchanged'
alias gcam='git commit -am'

alias gsl='git stash list'
gss() {
    git stash show -p stash@{"$@"} ;
}
gsa() {
    git stash apply stash@{"$@"} ;
}
gsd() { 
    git stash drop stash@{"$@"};
}

# Dumb aliases
alias web='cd /var/www/git'
alias please='sudo "$BASH" -c "$(history -p !!)"'
alias gc='grep --color'
alias cr='clear'
alias disk="du -h / 2> /dev/null | grep '[0-9\.]\+G'"
alias ls='ls -GSh --color' # enable colored output, sorting, and readable sizes
alias l='ls'
alias ldirs='ls -d */'
alias la='ls -Sla'
alias vi='vim'
alias v='vim'
alias ll='ls -l'
alias swp='cd ~/.swpfiles'
alias desk='cd ~/Desktop'
alias d='cd ~/dotfiles'
alias vv='v ~/.vimrc'
alias o='logout'
alias grep='grep -I --color'
alias gerp='grep'
alias gg='grep -inr --color'
alias skim="(head -5; tail -5) <"
alias files="cut -d: -f1 | uniq"
alias topstill='top -l 1 > /tmp/topout.txt; cd /tmp; v topout.txt'
alias sbp='source ~/.bashrc'
# Make these commands ask before clobbering a file. Use -f to override.
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias hc="cat /dev/null > ~/.bash_history"

# generate random pass
function password {
    date +%s | sha256sum | base64 | head -c 32 ; echo
}
# Laravel aliases
alias migrate='php artisan migrate'
alias routes='php artisan route:list' 

function mmt {
    php artisan make:migration "$1" --table="$2"
}
function mmc {
    php artisan make:migration "$1" --create="$2"
}
# nmap some stff
function nmapsso {
   sudo nmap -sS -O "$1"
}
function nmapsv {
   sudo nmap -sV "$1"
}
# Use `docker-cleanup --dry-run` to see what would be deleted.

function docker-cleanup {
  EXITED=$(docker ps -q -f status=exited)
    DANGLING=$(docker images -q -f "dangling=true")

    if [ "$1" == "--dry-run" ]; then
        echo "==> Would stop containers:"
        echo $EXITED
        echo "==> And images:"
        echo $DANGLING
    else
        if [ -n "$EXITED" ]; then
            docker rm $EXITED
        else
            echo "No containers to remove."
        fi
        if [ -n "$DANGLING" ]; then
            docker rmi $DANGLING
        else
            echo "No images to remove."
        fi
    fi
}

# Git add multiple remotes
function git-add-push-all() {
  while read -r name url method; do
      git config --add remote.all.url "$1"
    done < <(git remote -v | awk '!/^all/ && /push/')
}

# Show numeric rights in ls
function rights() {
    ls -l | awk '{k=0;for(i=0;i<=8;i++)k+=((substr($1,i+2,1)~/[rwx]/) \
                 *2^(8-i));if(k)printf("%0o ",k);print}'
}
