" pathogen plugin managing
execute pathogen#infect()
syntax on
syntax enable
colorscheme monokai
filetype plugin indent on

" Show hidden files
let NERDTreeShowHidden=1

" Powerline init
python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup

" Powerline statusline
set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/
set t_Co=256
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)

" Nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" scrooloose syntastic 
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
"let g:syntastic_phpcs_disable = 1 " what the fuck isnt it psr-2 complianti
" o wait it is
let g:syntastic_php_phpcs_args="--standard=PSR2 -n --report=csv" 

" PHP documenter script bound to Control-P
autocmd FileType php inoremap <C-p> <ESC>:call PhpDocSingle()<CR>i
autocmd FileType php nnoremap <C-p> :call PhpDocSingle()<CR>
autocmd FileType php vnoremap <C-p> :call PhpDocRange()<CR> 

" Editor options ripped from Laracasts
set showmode                    " always show what mode we're currently editing in
set nowrap                      " don't wrap lines
set tabstop=4                   " a tab is four spaces
set smarttab
set tags=tags
set softtabstop=4               " when hitting <BS>, pretend like a tab is removed, even if spaces
set expandtab                   " expand tabs by default (overloadable per file type later)
set shiftwidth=4                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set number                      " always show line numbers
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
set timeout timeoutlen=200 ttimeoutlen=100
set visualbell           " don't beep
set noerrorbells         " don't beep
set autowrite  "Save on buffer switch

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","
  
"  " Fast saves
nmap <leader>w :w!<cr>

" Down is really the next line
nnoremap j gj
nnoremap k gk
   
"Easy escaping to normal model
imap jj <esc>
     
"Auto change directory to match current file ,cd
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>
      
"easier window navigation
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

"easy buffer switching
nnoremap <F5> :buffers<CR>:buffer<Space>

"Resize vsplit
"nmap <C-v> :vertical resize +5<cr>
"nmap 25 :vertical resize 40<cr>
"nmap 50 <c-w>=
"nmap 75 :vertical resize 120<cr>
    
nmap <C-b> :NERDTreeToggle<cr>

"Show (partial) command in the status line
set showcmd
 
" Create split below
nmap :sp :rightbelow sp<cr>
   
" Quickly go forward or backward to buffer
nmap :bp :BufSurfBack<cr>
nmap :bn :BufSurfForward<cr>
   
highlight Search cterm=underline
     
" Swap files out of the project root
set backupdir=~/.vim/backups/
set directory=~/.swpfiles/

" Run PHPUnit tests
map <Leader>t :!clear && phpunit %<cr>

" Laravel cooool
map <Leader>r :!clear && php artisan route:list<CR>

autocmd cursorhold * set nohlsearch
autocmd cursormoved * set hlsearch
       
" Remove search results
command! H let @/=""
 
" If you prefer the Omni-Completion tip window to close when a selection is
" " made, these lines close it on movement in insert mode or when leaving
" " insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Command-T
noremap <leader>z :CommandT<CR>
noremap <leader>O :CommandTFlush<CR>
noremap <leader>m :CommandTBuffer<CR>

" CloseTags
autocmd FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1
autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako source ~/.vim/bundle/closetag/plugin/closetag.vim

" TagBar used with ctags! 
let g:tagbar_usearrows = 1
nnoremap <leader>l :TagbarToggle<CR>

" Map CTRL-C CTRL-P
vnoremap <C-c> "+y
vnoremap <C-p> "+p

" Cycle tabs
noremap <leader>a :tabp<CR>
noremap <leader>d :tabn<CR>

" php-autocomplete. Note: YCM installed seperately!!
"autocmd  FileType  php setlocal omnifunc=phpcomplete_extended#CompletePHP
autocmd  FileType  php setlocal omnifunc=phpcomplete#CompletePHP

" PHP autocompleter project based
let g:phpcomplete_index_composer_command="/usr/local/bin/composer"

" Open stuff in new tabs by default
"let NERDTreeMapOpenInTab='<C-z>'
"let g:CommandTAcceptSelectionMap = '<C-t>'
"let g:CommandTAcceptSelectionTabMap = '<CR>'
